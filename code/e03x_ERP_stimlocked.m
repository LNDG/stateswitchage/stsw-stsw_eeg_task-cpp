
% Plot early visual potentials and assess ERPs during stimulus processing

%% initialize

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

pn.root = pwd;
pn.tools = fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v3');
pn.plotFolder = fullfile(pn.root, 'figures', 'E');
    
addpath(fullfile(pn.root, 'tools'));
    % requires convertPtoExponential
    % requires mysigstar_vert
addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'));
addpath(fullfile(pn.root, 'tools', 'BrewerMap'));
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));

load(fullfile(pn.out, 'GrandAverages_v3.mat'), 'dataStimGrandAvg', 'IDs', 'ageIdx')

%% Stimulus-evoked SSVEP

h = figure('units','normalized','position',[.1 .1 .9 .4]);
subplot(1,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlim([-1000 3000]); xlabel('Time (ms); stim-locked'); ylim([-1.7*10^-3 1*10^-3])
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: Occipital ERP')
subplot(1,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlim([-1000 3000]); xlabel('Time (ms); stim-locked'); ylim([-1.7*10^-3 1*10^-3])
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: Occipital ERP')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'I_occipitalERP_STIM';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% restrict to first 500 ms

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlim([0 500]); xlabel('Time (ms); stim-locked'); ylim([-1.7*10^-3 1*10^-3])
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: Occipital ERP')
    line([50,50], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 0 0])
    line([100,100], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 0 0])
    line([130,130], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [1 .2 .2])
    line([160,160], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [1 .2 .2])
    line([180,180], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 .8 .5])
    line([220,220], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 .8 .5])
    line([280,280], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [.2 .2 1])
    line([320,320], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [.2 .2 1])
subplot(1,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlim([0 500]); xlabel('Time (ms); stim-locked'); ylim([-1.7*10^-3 1*10^-3])
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    xlim([0 500]);
    title('OA: Occipital ERP')
     line([50,50], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 0 0])
    line([100,100], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 0 0])
    line([130,130], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [1 .2 .2])
    line([160,160], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [1 .2 .2])
    line([180,180], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 .8 .5])
    line([220,220], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 .8 .5])
    line([280,280], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [.2 .2 1])
    line([320,320], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [.2 .2 1])
set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
% figureName = 'I_occipitalERP_STIM_Early';
% saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
% saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
% saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot topography 

cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'EastOutside';
cfg.style = 'both';
cfg.colormap = cBrew;

%% Early stimulus-evoked potentials

h = figure('units','normalized','position',[.1 .1 .7 .7]);
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    curData = squeeze(nanmean(cat(4, dataStimGrandAvg{1,1}.individual,dataStimGrandAvg{2,1}.individual,...
        dataStimGrandAvg{3,1}.individual, dataStimGrandAvg{4,1}.individual),4));
subplot(2,3,1)
    cfg.figure = h;
    cfg.zlim = [-3*10^-4 3*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>50 & dataStimGrandAvg{1,1}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 50:100 ms stim')
subplot(2,3,2)
    cfg.zlim = [-8*10^-4 8*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>130 & dataStimGrandAvg{1,1}.time<160;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 130:160 ms stim')
subplot(2,3,3)
    cfg.zlim = [-6*10^-4 6*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>180 & dataStimGrandAvg{1,1}.time<220;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 180:220 ms stim')
subplot(2,3,4)
    cfg.zlim = [-10*10^-4 10*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>280 & dataStimGrandAvg{1,1}.time<320;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 280:320 ms stim')
subplot(2,3,5)
    cfg.zlim = [-10*10^-4 10*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>320 & dataStimGrandAvg{1,1}.time<1000;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 320:1000 ms stim')
subplot(2,3,6)
    cfg.zlim = [-8*10^-4 8*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>2000 & dataStimGrandAvg{1,1}.time<3000;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 2000:3000 ms stim')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% figureName = 'I1_topoEarlyERP_YA';
% saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
% saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
% saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% Early potentials for OAs

h = figure('units','normalized','position',[.1 .1 .7 .7]);
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    curData = squeeze(nanmean(cat(4, dataStimGrandAvg{1,2}.individual,dataStimGrandAvg{2,2}.individual,...
        dataStimGrandAvg{3,2}.individual, dataStimGrandAvg{4,2}.individual),4));
subplot(2,3,1)
    cfg.zlim = [-3*10^-4 3*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>50 & dataStimGrandAvg{1,1}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 50:100 ms stim')
subplot(2,3,2)
    cfg.zlim = [-8*10^-4 8*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>130 & dataStimGrandAvg{1,1}.time<160;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 130:160 ms stim')
subplot(2,3,3)
    cfg.zlim = [-6*10^-4 6*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>180 & dataStimGrandAvg{1,1}.time<220;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 180:220 ms stim')
subplot(2,3,4)
    cfg.zlim = [-10*10^-4 10*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>280 & dataStimGrandAvg{1,1}.time<320;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 280:320 ms stim')
subplot(2,3,5)
    cfg.zlim = [-10*10^-4 10*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>320 & dataStimGrandAvg{1,1}.time<1000;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 320:1000 ms stim')
subplot(2,3,6)
    cfg.zlim = [-8*10^-4 8*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>2000 & dataStimGrandAvg{1,1}.time<3000;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 2000:3000 ms stim')
    
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% figureName = 'I1_topoEarlyERP_OA';
% saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
% saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
% saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% parametric increase around 300 ms

h = figure('units','normalized','position',[.1 .1 .7 .8]);
subplot(2,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[58:60],:)-dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[58:60],:)-dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[58:60],:)-dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
     xlim([0 500]); xlabel('Time (ms); stim-locked'); ylim([-0.3*10^-3 0.3*10^-3])
     legend({'2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: Occipital ERP')
subplot(2,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[58:60],:)-dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[58:60],:)-dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[58:60],:)-dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlabel('Time (ms); stim-locked'); ylim([-0.3*10^-3 0.3*10^-3])
    legend({'2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    xlim([0 500]);
    title('OA: Occipital ERP')
subplot(2,2,3);
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    cfg.zlim = [-3*10^-4 3*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>200 & dataStimGrandAvg{1,1}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,:,idxTime)-dataStimGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP 4-1: 200:300 ms')
subplot(2,2,4);
    cfg.zlim = [-3*10^-4 3*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>200 & dataStimGrandAvg{1,1}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,:,idxTime)-dataStimGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP 4-1: 200:300 ms')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% figureName = 'I_occipitalERP_STIM_Early_modulation';
% saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
% saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot early ERP increase for YA

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);

dataStimGrandAvgTotal = cat(4, dataStimGrandAvg{1,1}.individual, dataStimGrandAvg{2,1}.individual, ...
    dataStimGrandAvg{3,1}.individual, dataStimGrandAvg{4,1}.individual);

h = figure('units','normalized','position',[.1 .1 .7 .6]);
subplot(2,1,1); cla;
hold on;
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,1),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    xlim([-1000 3100])
    title('ERP magnitude increases alongside divided attention demands')
    xlabel('Time (ms from stimulus onset)'); ylabel('Amplitude (?Volt)')
	legend([l1.mainLine], {'Viusal ERP Load 1'}); legend('boxoff');
subplot(2,1,2);
hold on;
    condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],:,2:4)-repmat(dataStimGrandAvgTotal(:,[58:60],:,1),1,1,1,3),2),4));
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,2)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,3)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,4)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    
    % load linear regression statistics
    
%     load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/G_stimERP_CBPA_YA.mat', 'stat', 'cfgStat')
% 
%     significantLinear = stat.mask;
%     significantLinear = significantLinear*-1.5*10^-4;
%     significantLinear(significantLinear==0) = NaN;
%     plot(stat.time, significantLinear, 'k', 'LineWidth', 5)
    xlim([-1000 3100])
    legend([l2.mainLine, l3.mainLine, l4.mainLine], {'Load 2 - Load 1'; 'Load 3 - Load 1'; 'Load 4 - Load 1'}); legend('boxoff');
    title('ERP magnitude increases alongside divided attention demands')
    xlabel('Time (ms from stimulus onset)'); ylabel('Amplitude difference to selective attention')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'I_stimERPmodulation';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot early ERP increase for OA

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);

dataStimGrandAvgTotal = cat(4, dataStimGrandAvg{1,2}.individual, dataStimGrandAvg{2,2}.individual, ...
    dataStimGrandAvg{3,2}.individual, dataStimGrandAvg{4,2}.individual);

h = figure('units','normalized','position',[.1 .1 .7 .6]);
subplot(2,1,1); cla;
hold on;
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,1),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    xlim([-1000 3100])
    title('ERP magnitude increases alongside divided attention demands')
    xlabel('Time (ms from stimulus onset)'); ylabel('Amplitude (?Volt)')
	legend([l1.mainLine], {'Viusal ERP Load 1'}); legend('boxoff');
subplot(2,1,2);
hold on;
    condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],:,2:4)-repmat(dataStimGrandAvgTotal(:,[58:60],:,1),1,1,1,3),2),4));
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,2)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,3)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,4)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    
    % load linear regression statistics
    
%     load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/G_stimERP_CBPA_YA.mat', 'stat', 'cfgStat')
% 
%     significantLinear = stat.mask;
%     significantLinear = significantLinear*-1.5*10^-4;
%     significantLinear(significantLinear==0) = NaN;
%     plot(stat.time, significantLinear, 'k', 'LineWidth', 5)
    xlim([-1000 3100])
    legend([l2.mainLine, l3.mainLine, l4.mainLine], {'Load 2 - Load 1'; 'Load 3 - Load 1'; 'Load 4 - Load 1'}); legend('boxoff');
    title('ERP magnitude increases alongside divided attention demands')
    xlabel('Time (ms from stimulus onset)'); ylabel('Amplitude difference to selective attention')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'I_stimERPmodulation_OA';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
