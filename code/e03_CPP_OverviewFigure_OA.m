currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

pn.root = pwd;
pn.tools = fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v3');
pn.plotFolder = fullfile(pn.root, 'figures', 'E');
    
addpath(fullfile(pn.root, 'tools'));
    % requires mysigstar_vert
addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'));
addpath(fullfile(pn.root, 'tools', 'BrewerMap'));
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));

load(fullfile(pn.out, 'GrandAverages_v3.mat'), 'dataResponseGrandAvg', 'IDs', 'ageIdx')

%% Figure: Resp-locked CPP with insets

h = figure('units','normalized','position',[.1 .1 .4 .35]);
set(gcf,'renderer','Painters')
cla; hold on;
    
    cBrew = brewermap(6,'Blues');
    cBrew = cBrew(3:6,:);
    %cBrew = flipud(cBrew);
    
    %cBrew = [8.*[.1 .12 .12]; 6.*[.1 .15 .15]; 4.*[.1 .2 .2]; 2.*[.1 .3 .3]];

    catAll = cat(4,dataResponseGrandAvg{1,2}.individual,...
    dataResponseGrandAvg{2,2}.individual,dataResponseGrandAvg{3,2}.individual,...
    dataResponseGrandAvg{4,2}.individual);

% new value = old value ? subject average + grand average

    condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
    curData = squeeze(catAll(:,54,:,1));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataResponseGrandAvg{1,2}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataResponseGrandAvg{1,2}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataResponseGrandAvg{1,2}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataResponseGrandAvg{1,2}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);

    ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
    set(ll, 'Color', 'k')
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine],...
            {'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'South', 'Orientation', 'horizontal'); legend('boxoff');
    xlim([-1000 100]); xlabel('Time (ms); response-locked'); ylabel('ERP Amplitude @POz (?V)');
    title({'CPP slope decreases with prior target uncertainty'; ''})
    ylim([-1 7]*10^-4)

%% inset 1: bar plots of slope decrease with load
    
    dataForSlopeFit = []; slopes = [];
    for indAge = 1:2
        for indCond = 1:4
            dataForSlopeFit{indAge}(:,indCond,:) = squeeze(dataResponseGrandAvg{indCond,indAge}.individual(:,54,dataResponseGrandAvg{1,2}.time>-250 & dataResponseGrandAvg{1,2}.time<-50));
            for indID = 1:size(dataForSlopeFit{indAge},1)
                slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),squeeze(dataForSlopeFit{indAge}(indID,indCond,:))',1);
            end
        end
    end
    

%% insert 1: plot RainCloudPlots
 
    ax2 = axes('Position',[.2 .55 .18 .25]);
    box off;
    hold on;

    indGroup = 2;

    cBrew(1,:) = cBrew(4,:);
    cBrew(2,:) = cBrew(4,:);

    idx_outlier = cell(1); idx_standard = cell(1);
    dataToPlot = squeeze(slopes{indAge}(:,:,1));
    % define outlier as lin. modulation that is more than three scaled median absolute deviations (MAD) away from the median
    X = [1 1; 1 2; 1 3; 1 4]; b=X\dataToPlot'; IndividualSlopes = b(2,:);
    outliers = isoutlier(IndividualSlopes, 'median');
    idx_outlier{indGroup} = find(outliers);
    idx_standard{indGroup} = find(outliers==0);

    dataToPlot = squeeze(slopes{indAge}(:,:,1));
    % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = dataToPlot(:,i);
            % individually demean for within-subject visualization
            data_ws{i, j} = dataToPlot(:,i)-...
                nanmean(dataToPlot(:,:),2)+...
                repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
            data_nooutlier{i, j} = data{i, j};
            data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            data_ws_nooutlier{i, j} = data_ws{i, j};
            data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
            % sort outliers to back in original data for improved plot overlap
            data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cla;
    cl = cBrew(indGroup,:);
    [~, spacing] = rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1);
    h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],spacing);
    view([90 -90]);
    axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./2]);

    minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
    xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
    ylabel('Target load'); %xlabel({'Brainscore'; '[Individually centered]'})

    % test linear effect
    curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    M = mean(IndividualSlopes);    
    if M>10^-3; tit_m = ['M: ', num2str(round(M,2))]; else; tit_m = sprintf('M: = %.1e',M); end
    if p>10^-3; tit_p = ['p: ', num2str(round(p,2))]; else; tit_p = sprintf('p = %.1e',p); end
    title([tit_m, ',', tit_p])

 %% inset 2: topography
 
    axes('Position',[.8 .2 .3 .3])
    box off
 
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.zlim = [-3*10^-4 6*10^-4];
    cfg.colormap = cBrew;
    cfg.figure = h;
    plotData = [];
    plotData.label = dataResponseGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,2}.time>-250 & dataResponseGrandAvg{1,2}.time<100;
    grandAvg = cat(4, dataResponseGrandAvg{1,2}.individual, dataResponseGrandAvg{2,2}.individual, ...
        dataResponseGrandAvg{3,2}.individual, dataResponseGrandAvg{4,2}.individual);
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(grandAvg(:,:,idxTime,:),3),1),4))';
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','ERP amplitude (?V)');
    %title({'ERP (across loads): -250:100 ms peri-response'; ''})
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    
%% save CPP figure

    figureName = 'Z3_CPP_OA_RCP';
    saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
  
%% save estimated CPP slopes

    CPPslopes.data = slopes{2}(:,:,1);
    CPPslopes.IDs = IDs(ageIdx{2});

    save(fullfile(pn.root, 'data', 'Z_CPPslopes_OA.mat'), 'CPPslopes');
