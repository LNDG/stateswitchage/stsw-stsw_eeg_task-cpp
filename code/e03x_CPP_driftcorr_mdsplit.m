currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

pn.root = pwd;
pn.tools = fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v3');
pn.plotFolder = fullfile(pn.root, 'figures', 'E');
    
addpath(fullfile(pn.root, 'tools'));
    % requires mysigstar_vert
addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'));
addpath(fullfile(pn.root, 'tools', 'BrewerMap'));
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));

load(fullfile(pn.out, 'GrandAverages_v3.mat'), 'dataResponseGrandAvg', 'IDs', 'ageIdx')

%% plot correlation between HDDM- and CPP-derived drift estimates

file_sum = fullfile(pn.root, '..', '..', 'stsw_beh_task', 'd_hddm', 'data', 'HDDM_summary_v12_a2_t2.mat');
load(file_sum)

h = figure('units','normalized','position',[.1 .1 .5 .5]);

for indAge = 1:2
    
    if indAge == 1
        subplot(2,3,1)
    else
        subplot(2,3,4)
    end
        
    idx_insum = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{indAge}));

    % slope fit from -250 to -100 (McGovern et al., 2018)
    
    dataForSlopeFit = []; slopes = [];
    for indCond = 1:4
        dataForSlopeFit{indAge}(:,indCond,:) = squeeze(...
            dataResponseGrandAvg{indCond,indAge}.individual...
            (:,54,dataResponseGrandAvg{1,indAge}.time>-250 & ...
            dataResponseGrandAvg{1,indAge}.time<-100));
        for indID = 1:size(dataForSlopeFit{indAge},1)
            slopes{indAge}(indID,indCond,:) = polyfit(...
                1:size(dataForSlopeFit{indAge},3),...
                squeeze(dataForSlopeFit{indAge}(indID,indCond,:))',1);
        end
    end
    
    a = HDDM_summary_v12_a2_t2.driftEEG(idx_insum,1);
    b = squeeze(slopes{indAge}(:,1,1));
    l1 = scatter(a,b, 70,'k', 'filled'); l2 = lsline(); set(l2, 'LineWidth', 3);
    [r, p] = corrcoef(a,b);
    legend(l2, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))], ...
        'location', 'NorthWest'); legend('boxoff')
    title({'Relation between HDDM- & CPP-indicated drift rate'; ''})
    xlabel('HDDM drift (EEG) Load 1'); ylabel('CPP slope (EEG) Load 1'); 
    set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% plot median split of CPP based on HDDM-derived drift
    
    [~, sortInd] = sort(nanmean(HDDM_summary_v12_a2_t2.driftEEG(idx_insum,:),2), 'descend');
    
    % add within-subject error bars, do stats between fast and slow performers
    
    if indAge == 1
        subplot(2,3,[2,3]); cla;
    else
        subplot(2,3,[5,6]); cla;
    end
        
    % between-subject error bars
   
    curData = squeeze(dataResponseGrandAvg{1,indAge}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataResponseGrandAvg{1,indAge}.time,...
        nanmean(curData,1),standError, 'lineprops', ...
        {'color', cBrew(4,:), 'LineStyle', '-','linewidth', 2}, 'patchSaturation', .1);

    curData = squeeze(dataResponseGrandAvg{1,indAge}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataResponseGrandAvg{1,indAge}.time,...
        nanmean(curData,1),standError, 'lineprops', ...
        {'color', cBrew(1,:), 'LineStyle', '-','linewidth', 2}, 'patchSaturation', .1);

    curData = squeeze(dataResponseGrandAvg{4,indAge}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataResponseGrandAvg{1,indAge}.time,...
        nanmean(curData,1),standError, 'lineprops', ...
        {'color', cBrew(4,:), 'LineStyle', '--','linewidth', 2}, 'patchSaturation', .1);

    curData = squeeze(dataResponseGrandAvg{4,indAge}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataResponseGrandAvg{1,indAge}.time,...
        nanmean(curData,1),standError, 'lineprops', ...
        {'color', cBrew(1,:), 'LineStyle', '--','linewidth', 2}, 'patchSaturation', .1);
    
    xlim([-1000 100])
    ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
    set(ll, 'Color', 'k')
    legend([l1.mainLine, l3.mainLine, l2.mainLine, l4.mainLine],...
        {'Performers high drift Load 1'; 'Performers high drift Load 4'; ...
        'Performers slow drift Load 1'; 'Performers low drift Load 4'}, ...
        'location', 'NorthWest'); legend('boxoff');
    title({'CPP median split according to high/low HDDM drift across loads'; ''})
    set(findall(gcf,'-property','FontSize'),'FontSize',22)
    xlabel('Time (ms, response-locked)'); ylabel('ERP Amplitude @POz (?V)');
    
end

set(findall(h,'-property','FontSize'),'FontSize',15)

%% median-split of the probe-aligned data (OA

h = figure('units','normalized','position',[.1 .1 .4 .4]);
% between-subject error bars
cla;
curData = squeeze(dataProbeGrandAvg{1,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(dataProbeGrandAvg{1,2}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(4,:), 'LineStyle', '-','linewidth', 2}, 'patchSaturation', .1);
curData = squeeze(dataProbeGrandAvg{1,2}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l2 = shadedErrorBar(dataProbeGrandAvg{1,2}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(1,:), 'LineStyle', '-','linewidth', 2}, 'patchSaturation', .1);
curData = squeeze(dataProbeGrandAvg{4,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l3 = shadedErrorBar(dataProbeGrandAvg{1,2}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(4,:), 'LineStyle', '--','linewidth', 2}, 'patchSaturation', .1);
curData = squeeze(dataProbeGrandAvg{4,2}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(dataProbeGrandAvg{1,2}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(1,:), 'LineStyle', '--','linewidth', 2}, 'patchSaturation', .1);
xlim([-200 1000])
ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
set(ll, 'Color', 'k')
legend([l1.mainLine, l3.mainLine, l2.mainLine, l4.mainLine],...
    {'Performers high drift Load 1'; 'Performers high drift Load 4'; 'Performers slow drift Load 1'; 'Performers low drift Load 4'}, 'location', 'NorthWest'); legend('boxoff');
title('CPP median split according to high/low DDM drift')
set(findall(gcf,'-property','FontSize'),'FontSize',22)
xlabel('Time (ms, Probe-locked)'); ylabel('ERP Amplitude @POz (??Volts)');


%% plot YA and OA in single plot

% add within-subject error bars, do stats between fast and slow performers

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);

h = figure('units','normalized','position',[.1 .1 .4 .4]);

% between-subject error bars

cla;

curData = squeeze(dataResponseGrandAvg{1,2}.individual(:,54,:));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(dataResponseGrandAvg{1,2}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(4,:), 'LineStyle', '-','linewidth', 2}, 'patchSaturation', .1);


curData = squeeze(dataResponseGrandAvg{4,2}.individual(:,54,:));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l3 = shadedErrorBar(dataResponseGrandAvg{1,2}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(4,:), 'LineStyle', '--','linewidth', 2}, 'patchSaturation', .1);

hold on;       

curData = squeeze(dataResponseGrandAvg{1,1}.individual(:,54,:));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l2 = shadedErrorBar(dataResponseGrandAvg{1,2}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(1,:), 'LineStyle', '-','linewidth', 2}, 'patchSaturation', .1);

curData = squeeze(dataResponseGrandAvg{4,1}.individual(:,54,:));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(dataResponseGrandAvg{1,2}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(1,:), 'LineStyle', '--','linewidth', 2}, 'patchSaturation', .1);

xlim([-1000 100])
ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
set(ll, 'Color', 'k')
legend([l1.mainLine, l3.mainLine, l2.mainLine, l4.mainLine],...
    {'OA Load 1'; 'OA Load 4'; 'YA Load 1'; 'YA Load 4'}, 'location', 'NorthWest'); legend('boxoff');
title('CPP median split according to high/low DDM drift')
set(findall(gcf,'-property','FontSize'),'FontSize',22)
xlabel('Time (ms, response-locked)'); ylabel('ERP Amplitude @POz (microVolts)');

figureName = 'E_CPP_L1L4_YAOA';
saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% check whether median split according to L1 also differentiates L4 performance
% yes it does, which also hints at the high reliability across
% conditions; subjects become more similarly bad performers, but in a
% somewhat stable rank order

[~, sortInd] = sort(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1), 'descend');

idx_OA = find(idx_OA);

a = HDDM_summary_v12_a2_t2.driftEEG(idx_OA(sortInd(ceil(numel(sortInd)/2)+1:end)),4);
b = HDDM_summary_v12_a2_t2.driftEEG(idx_OA(sortInd(1:ceil(numel(sortInd)/2))),4);

[h,p] = ttest2(a,b)

a = HDDM_summary_v12_a2_t2.driftEEG(idx_OA(sortInd(ceil(numel(sortInd)/2)+1:end)),1);
b = HDDM_summary_v12_a2_t2.driftEEG(idx_OA(sortInd(1:ceil(numel(sortInd)/2))),1);

[h,p] = ttest2(a,b)

%% between-subject group t-tests good vs. bad performers

% create different structures for the two groups

idx_performance{1} = sortInd(1:ceil(numel(sortInd)/2));
idx_performance{2} = sortInd(ceil(numel(sortInd)/2)+1:end);
for indGroup = 1:2
    StatStruct{1,indGroup} = dataResponseGrandAvg{1,2};
    StatStruct{1,indGroup}.powspctrm = dataResponseGrandAvg{1,2}.individual(idx_performance{indGroup},54,:);
    StatStruct{1,indGroup}.label = [];
    StatStruct{1,indGroup}.label{1} = 'CPP';
    %StatStruct{1,indGroup}.dimord = 'subj_chan_time';
    StatStruct{1,indGroup}= rmfield(StatStruct{1,indGroup}, 'individual');
    StatStruct{2,indGroup} = dataResponseGrandAvg{4,2};
    StatStruct{2,indGroup}.powspctrm = dataResponseGrandAvg{4,2}.individual(idx_performance{indGroup},54,:);
    %StatStruct{2,indGroup}.dimord = 'subj_time';
    StatStruct{2,indGroup}= rmfield(StatStruct{2,indGroup}, 'individual');
    StatStruct{2,indGroup}.label = [];
    StatStruct{2,indGroup}.label{1} = 'CPP';
end

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_indepsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = [];

N_1 = size(StatStruct{1,2}.powspctrm,1);
N_2 = size(StatStruct{1,2}.powspctrm,1);
design = zeros(2,N_1+N_2);
design(1,1:N_1) = 1;
design(1,N_1+1:end) = 2;

cfgStat.design   = design;
cfgStat.ivar     = 1;

[stat] = ft_timelockstatistics(cfgStat, StatStruct{1,2}, StatStruct{1,2});
