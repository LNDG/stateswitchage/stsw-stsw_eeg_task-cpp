currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))

pn.root = pwd;
pn.tools = fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v3');
pn.plotFolder = fullfile(pn.root, 'figures', 'E');
    
addpath(fullfile(pn.root, 'tools'));
    % requires convertPtoExponential
    % requires mysigstar_vert
addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'));
addpath(fullfile(pn.root, 'tools', 'BrewerMap'));
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));

%% get individual CPPs

filename = fullfile(pn.root, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};
IDs = IDs(cellfun(@str2num, IDs, 'un', 1)<2000);

ERPdata = [];
for indID = 1:numel(IDs)
    load(fullfile(pn.out, [IDs{indID},'_ERP.mat']));
    time = dataProbeAvg{2,1}.time;
    for indDim = 1:4
       for indChan = 1:60
            ERPdata(indID,indDim,indChan,:) = squeeze(nanmean(dataResponseAvg{indDim,1}.avg(indChan,:),1));
       end
    end
end

%% load DDM estimates

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_YA_vt.mat')

DDMIDidx = ismember(cell2mat(cellfun(@str2num, HDDM_summary.IDs, 'un', 0)), cell2mat(cellfun(@str2num, IDs, 'un', 0)));
DDMIDidx = find(DDMIDidx);

%% event-lock to average (!) NDT in each condition
% This also works for individual NDTs but seems slightly noisier

% normalize via 1 target condition

ERPdata_norm = ERPdata;

ERPdata_NDTlocked = []; ERPdata_nonNDTlocked = [];
for indID = 1:numel(IDs)
    for indDim = 1:4
       curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),indDim),1));
       curNDT1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),1),1));
       timeIdx = find(time>-1000.*(curNDT-curNDT1),1,'first');
       timeIdx_nonNDT = find(time>=0,1,'first');
       ERPdata_NDTlocked(indID,indDim,:,:) = ERPdata_norm(indID,indDim,:,timeIdx-250:timeIdx+250);
       ERPdata_nonNDTlocked(indID,indDim,:,:) = ERPdata_norm(indID,indDim,:,timeIdx_nonNDT-250:timeIdx_nonNDT+250);
    end
end

NewTime = -250*2:2:250*2;

%% plot with within-subject errorbars

% add within-subject error bars

frontalCluster = [4,5,9:14,19:22];
%frontalClusterNeg = [7,16,17,25,26]; % negative

h = figure('units','normalized','position',[.1 .1 .2 .4]);
set(gcf,'renderer','Painters')
subplot(2,1,1); cla; hold on;
    for indCond = 1:4
        % new value = old value ?? subject average + grand average
        time = NewTime;
        condAvg = squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(:,1:4,[frontalCluster],:),3),2));
        curData = squeeze(nanmean(ERPdata_nonNDTlocked(:,indCond,[frontalCluster],:),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        line{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(indCond,:),'linewidth', 3}, 'patchSaturation', .1);
    end
    xlim([-500 500]); ylim([-4*10^-4 6*10^-4])
    %hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
    xlabel('Time (ms from response)'); ylabel('ERP amplitude (??V)')
    title({'NDT-locked motor preparation';''})
    meanNDTTime = 0;
    davinci('arrow', 'X', [meanNDTTime meanNDTTime], 'Y', [4.8*10^-4 2*10^-4], 'ArrowType', 'single', 'Head.Length', ...
        abs(diff([4.8*10^-4 2*10^-4]).*.5),'Head.Width', 40, 'Shaft.Width', 10)
subplot(2,1,2); cla; hold on;
    for indCond = 1:4
        % new value = old value ?? subject average + grand average
        time = NewTime;
        condAvg = squeeze(nanmean(nanmean(ERPdata_NDTlocked(:,1:4,[frontalCluster],:),3),2));
        curData = squeeze(nanmean(ERPdata_NDTlocked(:,indCond,[frontalCluster],:),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(indCond,:),'linewidth', 3}, 'patchSaturation', .1);
        meanNDTTime = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),indCond),1))*1000-...
            squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),1),1))*1000;
        davinci('arrow', 'X', [meanNDTTime meanNDTTime], 'Y', [4.8*10^-4 2*10^-4], 'ArrowType', 'single', 'Head.Length', ...
            abs(diff([4.8*10^-4 2*10^-4]).*.5), 'Head.Width', 40, 'Shaft.Width', 10, 'FaceColor', cBrew(indCond,:));
    end
    xlim([-500 500]); ylim([-4*10^-4 6*10^-4])
    legend([l{1}.mainLine, l{2}.mainLine, l{3}.mainLine, l{4}.mainLine],....
        {'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest')
    legend('boxoff');
    %hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
    xlabel('Time (ms from condition-specific residual NDT)'); ylabel('ERP amplitude (??V)')
    %title({'Grand average NDT-locked CPP';''})
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'E7_MotorPreparation_NDT_YA';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');
