%% load grand averages

    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..', '..'))

    pn.root = pwd;
    pn.tools = fullfile(pn.root, 'tools');
        addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
    pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v3');
    pn.plotFolder = fullfile(pn.root, 'figures', 'E');

    addpath(fullfile(pn.root, 'tools'));
        % requires convertPtoExponential
        % requires mysigstar_vert
    addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'));
    addpath(fullfile(pn.root, 'tools', 'BrewerMap'));
    addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));

    load(fullfile(pn.out, 'GrandAverages_v3.mat'), 'dataResponseGrandAvg', 'IDs', 'ageIdx')

    %% build stats matrices
    
    CBPAstruct = [];
    indGroup = 1;
    for indCond = 1:4
        CBPAstruct{indCond} = dataResponseGrandAvg{indCond,indGroup};
        CBPAstruct{indCond}.dimord = 'subj_chan_time';
        CBPAstruct{indCond}.time = CBPAstruct{indCond}.time;
        CBPAstruct{indCond}.individual = CBPAstruct{indCond}.individual;
    end
    
%% CBPA of pre-response potential: linear loadings

    cfg = [];
    cfg.method           = 'montecarlo';
    cfg.statistic        = 'ft_statfun_depsamplesregrT';
    cfg.latency          = [-200 0];
    cfg.avgovertime      = 'yes';
    cfg.correctm         = 'cluster';
    cfg.clusteralpha     = 0.05;
    cfg.clusterstatistic = 'maxsum';
    %cfg.minnbchan        = 2;
    cfg.tail             = 1;
    cfg.clustertail      = 1;
    cfg.alpha            = 0.05;
    cfg.numrandomization = 1000; % number of iterations

    % prepare_neighbours determines what sensors may form clusters
    cfg_neighb.method       = 'template';
    cfg_neighb.template     = 'elec1010_neighb.mat';
    cfg_neighb.channel      = CBPAstruct234.label;
    cfg.neighbours          = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1}); % use original dataset
    clear cfg_neighb;

    % specify design
    subj = size(CBPAstruct{1}.individual,1);
    conds = 4;
    design = zeros(2,conds*subj);
    for i = 1:subj
        for icond = 1:conds
            design(1,(icond-1)*subj+i) = i;
        end
    end
    for icond = 1:conds
        design(2,(icond-1)*subj+1:icond*subj) = icond;
    end

    cfg.design = design; clear design subj;
    cfg.uvar  = 1;
    cfg.ivar  = 2;
    cfg.parameter = 'individual'; % specify the field in which relevant data is contained

    [stat] = ft_timelockstatistics(cfg, CBPAstruct{1}, CBPAstruct{2}, CBPAstruct{3},CBPAstruct{4});
    
     %% topography of effect

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'SouthOutside';
    cfg.zlim = [-6 6];

    h = figure('units','normalized','position',[.1 .1 .2 .2]);
        cfg.highlight = 'on';
        cfg.highlightchannel = find(stat.mask);
        plotData = [];
        plotData.label = stat.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = stat.stat;
        ft_topoplotER(cfg,plotData);
        title({'ERP amplitude increases in frontal cluster';'with load prior to response'})
        cb = colorbar(cfg.colorbar); set(get(cb,'title'),'string','t-values');
        addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
        cBrew = brewermap(500,'RdBu');
        cBrew = flipud(cBrew);
        colormap(cBrew)
        set(findall(gcf,'-property','FontSize'),'FontSize',20)
        
    figureName = 'E5_CBPA_topography';
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% extract frontal channel cluster

    figure; plot(stat.posclusterslabelmat) % frontal cluster = 1; posterior cluster = 2
    figure; plot(stat.negclusterslabelmat) % frontal cluster = 1; posterior cluster = 2

    find(stat.posclusterslabelmat==1)'

    frontalCluster = [4,5,9:14,19:22];