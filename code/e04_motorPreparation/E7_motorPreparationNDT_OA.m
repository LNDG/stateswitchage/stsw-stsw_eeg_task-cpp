addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);

IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

ERPdata = [];
for indID = 1:numel(IDs)
    load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v2/',IDs{indID},'_ERP.mat']);
    time = dataProbeAvg{2,1}.time;
    for indDim = 1:4
       for indChan = 1:60
            ERPdata(indID,indDim,indChan,:) = squeeze(nanmedian(dataResponseAvg{indDim,1}.avg(indChan,:),1));
       end
    end
end

%% load DDM estimates

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_wSession_summary.mat')

DDMIDidx = ismember(cell2mat(cellfun(@str2num, HDDM_summary.IDs, 'un', 0)), cell2mat(cellfun(@str2num, IDs, 'un', 0)));
DDMIDidx = find(DDMIDidx);

%% event-lock to average (!) NDT in each condition
% This also works for individual NDTs but seems slightly noisier

% normalize via 1 target condition

ERPdata_norm = ERPdata;%-repmat(nanmedian(ERPdata(:,1,:,:),2),1,4,1,1);

ERPdata_NDTlocked = []; ERPdata_nonNDTlocked = [];
for indID = 1:numel(IDs)
    for indDim = 1:4
%        curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(indID),indDim),1));
%        curNDT1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(indID),1),1));
       curNDT = squeeze(nanmean(HDDM_wSession_summary.nondecisionEEG(DDMIDidx(:),indDim),1));
       curNDT1 = squeeze(nanmean(HDDM_wSession_summary.nondecisionEEG(DDMIDidx(:),1),1));
       timeIdx = find(time>-1000.*(curNDT-curNDT1),1,'first');
       timeIdx_nonNDT = find(time>=0,1,'first');
       ERPdata_NDTlocked(indID,indDim,:,:) = ERPdata_norm(indID,indDim,:,timeIdx-250:timeIdx+250);
       ERPdata_nonNDTlocked(indID,indDim,:,:) = ERPdata_norm(indID,indDim,:,timeIdx_nonNDT-250:timeIdx_nonNDT+250);
    end
end

NewTime = -250*2:2:250*2;

frontalCluster = [4,5,9:14,19:22];
%frontalCluster = [7,16,17,25,26]; % negative

% h = figure('units','normalized','position',[.1 .1 .2 .4]);
% subplot(2,1,1); cla; hold on;
%     for indCond = 1:4
%         plot(NewTime, squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(:,indCond,[frontalCluster],:),3),1))-...
%             squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(:,indCond,[1:23],:),3),1)), 'LineWidth', 3, 'Color', cBrew(indCond,:));
%     end
%     xlim([-500 500]); ylim([-2*10^-4 6*10^-4])
%     %hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
%     xlabel('Time (ms from response)'); ylabel('ERP amplitude (?V)')
%     title({'NDT-locked motor preparation';''})
%     meanNDTTime = 0;
%     davinci('arrow', 'X', [meanNDTTime meanNDTTime], 'Y', [4.8*10^-4 2*10^-4], 'ArrowType', 'single', 'Head.Length', ...
%         abs(diff([4.8*10^-4 2*10^-4]).*.5),'Head.Width', 40, 'Shaft.Width', 10)
% subplot(2,1,2); cla; hold on;
%     for indCond = 1:4
%         l{indCond} = plot(NewTime, squeeze(nanmean(nanmean(ERPdata_NDTlocked(:,indCond,[frontalCluster],:),3),1))-...
%             squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(:,indCond,[1:23],:),3),1)), 'LineWidth', 3, 'Color', cBrew(indCond,:));
%         meanNDTTime = squeeze(nanmean(HDDM_wSession_summary.nondecisionEEG(DDMIDidx(:),indCond),1))*1000-...
%             squeeze(nanmean(HDDM_wSession_summary.nondecisionEEG(DDMIDidx(:),1),1))*1000;
%         davinci('arrow', 'X', [meanNDTTime meanNDTTime], 'Y', [4.8*10^-4 2*10^-4], 'ArrowType', 'single', 'Head.Length', ...
%             abs(diff([4.8*10^-4 2*10^-4]).*.5), 'Head.Width', 40, 'Shaft.Width', 10, 'FaceColor', cBrew(indCond,:));
%     end
%     xlim([-500 500]); ylim([-3*10^-4 6*10^-4])
%     legend([l{1}, l{2}, l{3}, l{4}],{'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest')
%     legend('boxoff');
%     %hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
%     xlabel('Time (ms from condition-specific residual NDT)'); ylabel('ERP amplitude (?V)')
%     %title({'Grand average NDT-locked CPP';''})
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/';
% figureName = 'E7_MotorPreparation_NDT';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');

%% Revised (correct) version

h = figure('units','normalized','position',[.1 .1 .2 .4]);
subplot(2,1,1); cla; hold on;
    for indCond = 1:4
        % new value = old value µ subject average + grand average
        time = NewTime;
        condAvg = squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(:,1:4,[frontalCluster],:),3),2));
        curData = squeeze(nanmean(ERPdata_nonNDTlocked(:,indCond,[frontalCluster],:),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        line{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(indCond,:),'linewidth', 3}, 'patchSaturation', .1);
    end
    xlim([-500 500]); ylim([-4*10^-4 6*10^-4])
    %hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
    xlabel('Time (ms from response)'); ylabel('ERP amplitude (µV)')
    title({'NDT-locked motor preparation';''})
    meanNDTTime = 0;
    davinci('arrow', 'X', [meanNDTTime meanNDTTime], 'Y', [4.8*10^-4 2*10^-4], 'ArrowType', 'single', 'Head.Length', ...
        abs(diff([4.8*10^-4 2*10^-4]).*.5),'Head.Width', 40, 'Shaft.Width', 10)
subplot(2,1,2); cla; hold on;
    for indCond = 1:4
        % new value = old value µ subject average + grand average
        time = NewTime;
        condAvg = squeeze(nanmean(nanmean(ERPdata_NDTlocked(:,1:4,[frontalCluster],:),3),2));
        curData = squeeze(nanmean(ERPdata_NDTlocked(:,indCond,[frontalCluster],:),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(indCond,:),'linewidth', 3}, 'patchSaturation', .1);
        meanNDTTime = squeeze(nanmean(HDDM_wSession_summary.nondecisionEEG(DDMIDidx(:),indCond),1))*1000-...
            squeeze(nanmean(HDDM_wSession_summary.nondecisionEEG(DDMIDidx(:),1),1))*1000;
        davinci('arrow', 'X', [meanNDTTime meanNDTTime], 'Y', [4.8*10^-4 2*10^-4], 'ArrowType', 'single', 'Head.Length', ...
            abs(diff([4.8*10^-4 2*10^-4]).*.5), 'Head.Width', 40, 'Shaft.Width', 10, 'FaceColor', cBrew(indCond,:));
    end
    xlim([-500 500]); ylim([-4*10^-4 6*10^-4])
    legend([l{1}.mainLine, l{2}.mainLine, l{3}.mainLine, l{4}.mainLine],....
        {'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest')
    legend('boxoff');
    %hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
    xlabel('Time (ms from condition-specific residual NDT)'); ylabel('ERP amplitude (µV)')
    %title({'Grand average NDT-locked CPP';''})
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/';
figureName = 'E7_MotorPreparation_NDT_OA';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
