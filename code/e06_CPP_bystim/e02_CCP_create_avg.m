% Create across-subject averages by condition and group

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))

pn.root     = pwd;
pn.tools	= fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v4');

%% create or load grand averages by group

disp('create and save grand averages')

filename = fullfile(pn.root, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

ageLabel{1} = 'YA'; ageLabel{2} = 'OA';

for indAge = 1:2
    curIDs = IDs(ageIdx{indAge});

    for id = 1:length(curIDs)
        load(fullfile(pn.out, [curIDs{id}, '_ERP.mat']), ...
            'dataStimAvg', 'dataProbeAvg', 'dataResponseAvg');
        ERPstruct.dataStimAvg(:,:,id) = dataStimAvg;
        ERPstruct.dataProbeAvg(:,:,id) = dataProbeAvg;
        ERPstruct.dataResponseAvg(:,:,id) = dataResponseAvg;
    end

    cfg = [];
    for indCond = 1:4
        for indStim = 1:4
            cfg.keepindividual = 'yes';
            dataStimGrandAvg{indCond,indStim,1} = ft_timelockgrandaverage(cfg, ...
                ERPstruct.dataStimAvg{indCond,indStim,:});
            dataProbeGrandAvg{indCond,indStim,1} = ft_timelockgrandaverage(cfg, ...
                ERPstruct.dataProbeAvg{indCond,indStim,:});
            dataResponseGrandAvg{indCond,indStim,1} = ft_timelockgrandaverage(cfg, ...
                ERPstruct.dataResponseAvg{indCond,indStim,:});
        end
    end

    %% save grand averages

    save(fullfile(pn.out, ['dataStimGrandAvg_v4_',ageLabel{indAge},'.mat']), ...
        'dataStimGrandAvg', 'curIDs')
    save(fullfile(pn.out, ['dataProbeGrandAvg_v4_',ageLabel{indAge},'.mat']), ...
        'dataProbeGrandAvg', 'curIDs')
    save(fullfile(pn.out, ['dataResponseGrandAvg_v4_',ageLabel{indAge},'.mat']), ...
        'dataResponseGrandAvg', 'curIDs')
end
