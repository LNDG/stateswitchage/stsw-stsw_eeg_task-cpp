%% initialize

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))

pn.root     = pwd;
pn.tools	= fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools,'shadedErrorBar'));
    addpath(fullfile(pn.tools,'BrewerMap'));
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v4');
pn.plotFolder = fullfile(pn.root, 'figures', 'E');

%% load grand averages

load(fullfile(pn.out, 'dataStimGrandAvg_v4_YA.mat'), 'dataStimGrandAvg', 'curIDs')
load(fullfile(pn.out, 'dataProbeGrandAvg_v4_YA.mat'), 'dataProbeGrandAvg')
load(fullfile(pn.out, 'dataResponseGrandAvg_v4_YA.mat'), 'dataResponseGrandAvg')

%% Figure S2e: CPP by probed feature & target load

cBrew = brewermap(4,'RdBu');

l1 = []; l2 = []; l3 = []; l4 = [];
h = figure('units','normalized','position',[.1 .1 .2 .35]);
set(gcf,'renderer','Painters')
for indStim = 1:4
    hold on;
    cBrew = brewermap(4,'RdBu');
    catAll = cat(4,dataResponseGrandAvg{1,indStim,1}.individual,...
    dataResponseGrandAvg{2,indStim,1}.individual,dataResponseGrandAvg{3,indStim,1}.individual,...
    dataResponseGrandAvg{4,indStim,1}.individual);

    condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
    curData = squeeze(catAll(:,54,:,1));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1{indStim} = shadedErrorBar(dataResponseGrandAvg{1,indStim,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(indStim,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    l4{indStim} = shadedErrorBar(dataResponseGrandAvg{4,indStim,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(indStim,:),'linewidth', 2, 'LineStyle', '--'}, 'patchSaturation', .25);
end
ll1  = line([ 0 0], [ -4*10^-4 12*10^-4], 'LineStyle', ':', 'LineWidth', 2);
ll2  = line([ -1000 100], [ 0 0], 'LineStyle', ':', 'LineWidth', 2);
set(ll1, 'Color', 'k')
set(ll2, 'Color', 'k')
ylim([-1.5*10^-4 12*10^-4])
title({'CPP slope as a function of stimulus and load'; ''})
legend([l1{1}.mainLine, l1{2}.mainLine, l1{3}.mainLine, l1{4}.mainLine],...
            {'Color'; 'Direction'; 'Size'; 'Luminance'}, 'location', 'NorthWest', 'Orientation', 'vertical'); legend('boxoff');
xlim([-1000 100]); xlabel('Time (ms); response-locked'); ylabel('ERP Amplitude @POz (?V)');  
    set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = 'F2_CPPbyStim';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% extract slopes and thresholds for each feature and their modulations

dataForSlopeFit = []; slopes = [];
for indAge = 1:2
    for indCond = 1:4
        for indStim = 1:4
            dataForSlopeFit{indAge}(:,indCond,indStim,:) = squeeze(dataResponseGrandAvg{indCond,indStim,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-250 & dataResponseGrandAvg{1,1}.time<-50));
            for indID = 1:size(dataForSlopeFit{indAge},1)
                slopes{indAge}(indID,indCond,indStim,:) = polyfit(1:size(dataForSlopeFit{indAge},4),squeeze(dataForSlopeFit{indAge}(indID,indCond,indStim,:))',1);
            end
        end
    end
end
dataForThreshold = [];
for indAge = 1:2
    for indCond = 1:4
        for indStim = 1:4
            curTime = dataResponseGrandAvg{1,1,1}.time;
            dataForThreshold{indAge}(:,indCond,indStim) = squeeze(nanmean(nanmean(dataResponseGrandAvg{indCond,indStim,indAge}.individual(:,[54],curTime>-50 & curTime<50),2),3));
        end
    end
end

%% calculate linear beta slopes for each feature, test vs. zero

for indFeature = 1:4
    data = slopes{1}(:,1:4,indFeature);
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects(:,indFeature) = b(2,:);
end

h = figure('units','normalized','position',[.1 .1 .4 .2]);
set(gcf,'renderer','Painters')
subplot(1,2,1); cla; hold on;
    condPairsLevel = [-3 -3 -3 -3]*10^-6;
    dat = LinearLoadEffects;
    bar(1:4, nanmean(dat), 'FaceColor', [0.0196, 0.4431, 0.6902], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color', 'Direction', 'Size', 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([-3.1 0]*10^-6)
    ylabel({'CPP slope';'[linear load beta]'}); xlabel('Probed Attribute');
    for indPair = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat(:,indPair)); % paired t-test vs 0
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [indPair], condPairsLevel(indPair), pval);
        end
    end
    
% plot linear slopes also for threshold

for indFeature = 1:4
    data = dataForThreshold{1}(:,1:4,indFeature);
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects(:,indFeature) = b(2,:);
end

subplot(1,2,2); cla; hold on;
    condPairsLevel = [2.75, 2.75, 2.75, 2.75]*10^-4;

    dat = LinearLoadEffects;
    bar(1:4, nanmean(dat), 'FaceColor',  [.5 .5 .5], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color', 'Direction', 'Size', 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([-6 6]*10^-5)
    ylabel({'CPP threshold';'[linear load beta]'}); xlabel('Probed Attribute');
    for indPair = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat(:,indPair)); % paired t-test vs 0
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [indPair], condPairsLevel(indPair), pval);
        end
    end
set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = 'F2_Slope_Thresh_Modulation_byProbe';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% plot differences between stimuli

condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
condPairsLevel = [10*10^-6 10.2*10^-6 8.75*10^-6 9.3*10^-6 9*10^-6 9.5*10^-6];

h = figure('units','normalized','position',[.1 .1 .4 .2]);
set(gcf,'renderer','Painters')
subplot(1,2,1); hold on;
    dat = squeeze(nanmean(slopes{1}(:,1,:,1),2));
    bar(1:4, nanmean(dat), 'FaceColor',  [.2 .6 .2], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color'; 'Direction'; 'Size'; 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([0 10.5*10^-6])
    ylabel('CPP slope'); xlabel('Probed Feature');
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end

condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
condPairsLevel = [8.3*10^-4 8.9*10^-4 7.75*10^-4 8.3*10^-4 8.5*10^-4 8.5*10^-4];

subplot(1,2,2); cla; hold on;
    dat = squeeze(nanmean(dataForThreshold{indAge}(:,1,:),2));
    bar(1:4, nanmean(dat), 'FaceColor',  [.2 .6 .2], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color'; 'Direction'; 'Size'; 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([0 9*10^-4])
    ylabel('CPP Threshold (?V)'); xlabel('Probed Feature');
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
set(findall(gcf,'-property','FontSize'),'FontSize',23)

figureName = 'F2_CPPbyStim_ThreshSlope';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% RainCloudPlot versions for the two prior plots

h = figure('units','normalized','position',[.1 .1 .4 .2]);
set(gcf,'renderer','Painters')
subplot(1,2,1); cla; hold on;
    condPairsLevel = [-10 -10 -10 -10]*10^-6;
    for indFeature = 1:4
        data = slopes{1}(:,1:4,indFeature);
        X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
        LinearLoadEffects(:,indFeature) = b(2,:);
    end
    dat = LinearLoadEffects; SourceData_slope = LinearLoadEffects;
    bar(1:4, nanmean(dat), 'FaceColor', [0.3, .6, .8], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard error on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 30);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color', 'Direction', 'Size', 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([-1.25 .9]*10^-5)
    ylabel({'CPP slope';'[linear load beta]'}); xlabel('Probed Attribute');
    for indPair = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat(:,indPair)); % paired t-test vs 0
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [indPair], condPairsLevel(indPair), pval);
        end
        % plot individual values on top
        scatter(repmat(indPair,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,indPair), ...
            30, 'filled', 'MarkerFaceColor', [0.0196, 0.4431, 0.6902]);
    end
    
% plot linear slopes also for threshold
subplot(1,2,2); cla; hold on;
    condPairsLevel = [2.75, 2.75, 2.75, 2.75]*10^-4;
    for indFeature = 1:4
        data = dataForThreshold{1}(:,1:4,indFeature);
        X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
        LinearLoadEffects(:,indFeature) = b(2,:);
    end
    dat = LinearLoadEffects; SourceData_thresh = LinearLoadEffects;
    bar(1:4, nanmean(dat), 'FaceColor',  [.7 .7 .7], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h2 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h2(1), 'marker', 'none'); % remove marker
    set(h2(2), 'LineWidth', 30);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color', 'Direction', 'Size', 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([-.7 1.1]*10^-3)
    ylabel({'CPP threshold';'[linear load beta]'}); xlabel('Probed Attribute');
    for indPair = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat(:,indPair)); % paired t-test vs 0
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [indPair], condPairsLevel(indPair), pval);
        end
        % plot individual values on top
        scatter(repmat(indPair,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,indPair), ...
            30, 'filled', 'MarkerFaceColor', [.2 .2 .2]);
    end
set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = 'F2_Slope_Thresh_Modulation_byProbe_RCP';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% SourceData

SourceData_slope
SourceData_thresh

%% plot differences between stimuli

h = figure('units','normalized','position',[.1 .1 .4 .2]);
set(gcf,'renderer','Painters')
subplot(1,2,1); hold on;
    condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
    condPairsLevel = [5*10^-6 5.2*10^-6 5.75*10^-6 5.3*10^-6 5*10^-6 5.5*10^-6].*-1;
    dat = squeeze(nanmean(slopes{1}(:,1,:,1),2));
    bar(1:4, nanmean(dat), 'FaceColor',  [.2 .6 .2], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color'; 'Direction'; 'Size'; 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([-1 2.5].*10^-5)
    ylabel('CPP slope'); xlabel('Probed Feature');
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
    % within-subject centering to emphasize between-condition differences
    dat = dat-nanmean(dat,2)+repmat(mean(mean(dat,2),1),size(dat,1),size(dat,2));
    for indCond = 1:4
        % plot individual values on top
        scatter(repmat(indCond,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,indCond), ...
            30, 'filled', 'MarkerFaceColor', [.2 .2 .2]);
    end
subplot(1,2,2); cla; hold on;
    condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
    condPairsLevel = [4.3*10^-4 4.9*10^-4 4.75*10^-4 4.3*10^-4 4.5*10^-4 4.5*10^-4].*-1;
    dat = squeeze(nanmean(dataForThreshold{1}(:,1,:),2));
    bar(1:4, nanmean(dat), 'FaceColor',  [.2 .6 .2], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color'; 'Direction'; 'Size'; 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([-.5 2].*10^-3)
    ylabel('CPP Threshold (?V)'); xlabel('Probed Feature');
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
    % within-subject centering to emphasize between-condition differences
    dat = dat-repmat(nanmean(dat,2),1,size(dat,2))+repmat(mean(mean(dat,2),1),size(dat,1),size(dat,2));
    for indCond = 1:4
        % plot individual values on top
        scatter(repmat(indCond,size(dat,1),1)+(rand(size(dat,1),1)-.5).*.4, dat(:,indCond), ...
            30, 'filled', 'MarkerFaceColor', [.2 .2 .2]);
    end
    
set(findall(gcf,'-property','FontSize'),'FontSize',23)

figureName = 'F2_CPPbyStim_ThreshSlope_RCP';
saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
saveas(h, fullfile(pn.plotFolder, figureName), 'png');

%% SourceData

SourceData_slope = squeeze(nanmean(slopes{1}(:,1,:,1),2));
SourceData_threshold = squeeze(nanmean(dataForThreshold{1}(:,1,:),2));

