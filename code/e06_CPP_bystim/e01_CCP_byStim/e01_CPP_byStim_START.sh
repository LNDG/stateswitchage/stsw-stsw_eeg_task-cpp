#!/bin/bash

fun_name="e01_CPP_byStim"
job_name="stsw_cpp"

mkdir ./logs

rootpath="$(pwd)/../../.."
rootpath=$(builtin cd $rootpath; pwd)

IDS=$(cat ${rootpath}/code/id_list.txt | tr '\n' ' ')

for subj in $IDS; do
	if [ ! -f ${rootpath}/data/E2_CCP_v4/${subj}_ERP.mat ]; then
  	sbatch \
  		--job-name ${job_name}_${subj} \
  		--cpus-per-task 4 \
  		--mem 4gb \
  		--time 01:00:00 \
  		--output ./logs/${job_name}_${subj}.out \
  		--workdir . \
  		--wrap=". /etc/profile ; module load matlab/R2016b; matlab -nodisplay -r \"${fun_name}('${subj}','${rootpath}')\""
  	fi
done