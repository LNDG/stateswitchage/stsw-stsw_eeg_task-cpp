function e01_CPP_byStim(id, rootpath)

    if ismac % run if function is not pre-compiled
        id = '1124'; % test for example subject
        currentFile = mfilename('fullpath');
        [pathstr,~,~] = fileparts(currentFile);
        cd(fullfile(pathstr,'..', '..', '..'))
        rootpath = pwd;
    end

    %% pathdef

    pn.dataIn	= fullfile(rootpath, '..', 'X1_preprocEEGData');
    pn.tools	= fullfile(rootpath, 'tools');
        addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
    pn.out	= fullfile(rootpath, 'data', 'E2_CCP_v4'); mkdir(pn.out)

    display(['processing ID ' id]);

    %% load data

    load(fullfile(pn.dataIn, [id, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat']), 'data');

    %% save TrlInfo from FT
    
    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;
    
    %% CSD transform (compute prior to averaging)
    
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    
    %% 8 Hz low-pass filter
    
    cfg = [];
    cfg.lpfilter = 'yes';
    cfg.lpfreq = 8;
    cfg.lpfiltord = 6;
    cfg.lpfilttype = 'but';
    cfg.lpfiltdir = 'twopass';
    
    data = ft_preprocessing(cfg,data);
    
    %% re-segment data with respect to stim onset, probe onset & response 
    % note that ft_redefinetrial unfortunately doesn't work due to the
    % time-locked structure
    
    % stim onset
    
    timepoint = repmat(3, size(data.trial,1),1);
    time = repmat(data.time{1}, size(data.trial,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-500;
    cfg.endsample = idx+2000;
    dataStim = ft_redefinetrial(cfg, data);
    
    % baseline: average of -250 to 0 ms prior to stim onset
    
    cfg = [];
    cfg.baseline = [2.75 3];
    [dataStim] = ft_timelockbaseline(cfg, dataStim);
    for indTrial = 1:numel(dataStim.trial)
        dataStim.time{1,indTrial} = -500*2:2:2000*2; % correct timing
    end
    
    %% baseline: average of -250 to 0 ms prior to probe onset
    
    cfg = [];
    cfg.baseline = [5.75 6];
    cfg.keepindividual = 'yes';
    [data] = ft_timelockbaseline(cfg, data);
    
    % probe onset
    
    timepoint = repmat(6.04, size(data.trial,1),1);
    time = repmat(data.time{1}, size(data.trial,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-500;
    cfg.endsample = idx+500;
    dataProbe = ft_redefinetrial(cfg, data);
    for indTrial = 1:numel(dataProbe.trial)
        dataProbe.time{1,indTrial} = -500*2:2:500*2;
    end
    
    % response
    
    timepoint = repmat(6.04+TrlInfo(:,9), size(data.trial,1),1);
    time = repmat(data.time{1}, size(data.trial,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-500;
    cfg.endsample = idx+500;
    trialToRemove = find(cfg.begsample<=0 | cfg.endsample>size(data.trial{1},2));
    cfg.trials = repmat(1,1,numel(data.trial)); cfg.trials(trialToRemove) = 0; cfg.trials = find(cfg.trials);
    dataResponse = ft_redefinetrial(cfg, data);
    for indTrial = 1:numel(dataResponse.trial)
        dataResponse.time{1,indTrial} = -500*2:2:500*2;
    end
    
    % remove empty trials from all other sturctures
    
    dataStim.trial(trialToRemove) = [];
    dataStim.time(trialToRemove) = [];
    dataProbe.trial(trialToRemove) = [];
    dataProbe.time(trialToRemove) = [];
    TrlInfo(trialToRemove,:) = [];
        
%% timelock to different events
    
    for indCond = 1:4
        for indStim = 1:4
            cfg = [];
            cfg.trials = TrlInfo(:,8)==indCond & TrlInfo(:,6) == indStim;
            % timelock to stim onset
            dataStimAvg{indCond,indStim} = ft_timelockanalysis(cfg, dataStim);
            % timelock to probe onset
            dataProbeAvg{indCond,indStim} = ft_timelockanalysis(cfg, dataProbe);
            % timelock to response
            dataResponseAvg{indCond,indStim} = ft_timelockanalysis(cfg, dataResponse);
        end
    end
    
    %% save computed files

    save(fullfile(pn.out, [id, '_ERP.mat']), 'dataStimAvg', 'dataProbeAvg', 'dataResponseAvg');
    