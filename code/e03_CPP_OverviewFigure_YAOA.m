currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

pn.root = pwd;
pn.tools = fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v3');
pn.plotFolder = fullfile(pn.root, 'figures', 'E');
    
addpath(fullfile(pn.root, 'tools'));
    % requires mysigstar_vert
addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'));
addpath(fullfile(pn.root, 'tools', 'BrewerMap'));
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));

load(fullfile(pn.out, 'GrandAverages_v3.mat'), 'dataResponseGrandAvg', 'IDs', 'ageIdx')

%% Figure: Resp-locked CPP with insets

h = figure('units','normalized','position',[.1 .1 .15 .2]);
subplot(1,1,1);
    set(gcf,'renderer','Painters')
    cla; hold on;

    cBrew = brewermap(6,'Blues');
    cBrew = cBrew(3:6,:);
    %cBrew = flipud(cBrew);

    %cBrew = [8.*[.1 .12 .12]; 6.*[.1 .15 .15]; 4.*[.1 .2 .2]; 2.*[.1 .3 .3]];

    time = dataResponseGrandAvg{1,1}.time;

    catAll = cat(4,dataResponseGrandAvg{1,1}.individual,...
    dataResponseGrandAvg{2,1}.individual,dataResponseGrandAvg{3,1}.individual,...
    dataResponseGrandAvg{4,1}.individual);

% new value = old value ? subject average + grand average

    condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
    curData = squeeze(catAll(:,54,:,1));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    ya_l1 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    SourceData_1M = nanmean(curData,1); SourceData_1SE = standError; SourceData_time = dataResponseGrandAvg{1,1}.time;

    curData = squeeze(catAll(:,54,:,4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    ya_l4 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    SourceData_4M = nanmean(curData,1); SourceData_4SE = standError; 

    %% add OAs

    cBrew = brewermap(6,'Reds');
    cBrew = cBrew(3:6,:);

    catAll = cat(4,dataResponseGrandAvg{1,2}.individual,...
    dataResponseGrandAvg{2,2}.individual,dataResponseGrandAvg{3,2}.individual,...
    dataResponseGrandAvg{4,2}.individual);

    catAll([20,39],:,:,:) = NaN;
% new value = old value ? subject average + grand average

    condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
    curData = squeeze(catAll(:,54,:,1));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    oa_l1 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    SourceData_1M = nanmean(curData,1); SourceData_1SE = standError; SourceData_time = dataResponseGrandAvg{1,1}.time;

    curData = squeeze(catAll(:,54,:,4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    oa_l4 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    SourceData_4M = nanmean(curData,1); SourceData_4SE = standError; 

    ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
    set(ll, 'Color', 'k')
    legend([ya_l1.mainLine, ya_l4.mainLine],...
            {'YA: 1 Target'; 'YA: 4 Targets'}, 'location', 'South', 'Orientation', 'horizontal'); legend('boxoff');
    xlim([-1000 100]); xlabel('Time (ms); response-locked'); ylabel('ERP Amplitude @POz (?V)');
    %ylim([-1.5 10]*10^-4);
    set(findall(gcf,'-property','FontSize'),'FontSize',16)

%% save CPP figure

    figureName = 'Z3_CPP_YAOA';
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
    
%% plot traces normalized to between 0 and 1

h = figure('units','normalized','position',[.1 .1 .1 .15]);
subplot(1,1,1);
    set(gcf,'renderer','Painters')
    cla; hold on;

    cBrew = brewermap(6,'Blues');
    cBrew = cBrew(3:6,:);

    time = dataResponseGrandAvg{1,1}.time;

    catAll = cat(4,dataResponseGrandAvg{1,1}.individual,...
    dataResponseGrandAvg{2,1}.individual,dataResponseGrandAvg{3,1}.individual,...
    dataResponseGrandAvg{4,1}.individual);

    for indCond = [1,4]
        curData = squeeze(catAll(:,54,:,indCond));
        curMean = nanmean(curData,1);
        rmin = repmat(nanmedian(curMean(time>-1000&time<-900)),1,1001);
        rmax = repmat(nanmedian(curMean(time>-100&time<100)),1,1001);
        curMean = (curMean-rmin)./(rmax-rmin);
        plot(time, curMean, 'color', cBrew(indCond,:),'linewidth', 3)
    end

    %% add OAs

    cBrew = brewermap(6,'Reds');
    cBrew = cBrew(3:6,:);

    catAll = cat(4,dataResponseGrandAvg{1,2}.individual,...
    dataResponseGrandAvg{2,2}.individual,dataResponseGrandAvg{3,2}.individual,...
    dataResponseGrandAvg{4,2}.individual);

    %catAll([20,39],:,:,:) = NaN;

    for indCond = [1,4]
        curData = squeeze(catAll(:,54,:,indCond));
        curMean = nanmean(curData,1);
        rmin = repmat(nanmedian(curMean(time>-1000&time<-900)),1,1001);
        rmax = repmat(nanmedian(curMean(time>-100&time<100)),1,1001);
        curMean = (curMean-rmin)./(rmax-rmin);
        plot(time, curMean, 'color', cBrew(indCond,:),'linewidth', 3)
    end
    
    ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
    set(ll, 'Color', 'k')
    xlim([-1000 100]); xlabel('Time to response (ms)'); ylabel('CPP (rescaled)');
    %ylim([-1.5 10]*10^-4);
    set(findall(gcf,'-property','FontSize'),'FontSize',16)
    
    figureName = 'Z3_CPP_YAOA_rescaled';
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
    
%% plot topography averaged across age groups

h = figure('units','normalized','position',[.1 .1 .15 .2]);
    set(gcf,'renderer','Painters')
    box off
 
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.zlim = [-3*10^-4 6*10^-4];
    cfg.colormap = cBrew;
    cfg.figure = h;
    plotData = [];
    plotData.label = dataResponseGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,1}.time>-250 & dataResponseGrandAvg{1,1}.time<100;
    grandAvg = cat(1,...
        cat(4, dataResponseGrandAvg{1,1}.individual, dataResponseGrandAvg{2,1}.individual, ...
            dataResponseGrandAvg{3,1}.individual, dataResponseGrandAvg{4,1}.individual), ...
        cat(4, dataResponseGrandAvg{1,2}.individual, dataResponseGrandAvg{2,2}.individual, ...
            dataResponseGrandAvg{3,2}.individual, dataResponseGrandAvg{4,2}.individual));
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(grandAvg(:,:,idxTime,:),3),1),4))';
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','ERP amplitude (?V)');
    %title({'ERP (across loads): -250:100 ms peri-response'; ''})
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    
    figureName = 'Z3_CPP_topo_YAOA';
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
    