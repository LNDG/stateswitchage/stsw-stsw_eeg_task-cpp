
currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))

%% pathdef

pn.root     = pwd;
pn.dataIn	= fullfile(pn.root, '..', 'X1_preprocEEGData');
pn.tools	= fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v5'); mkdir(pn.out)

%% define IDs

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for id = 1:length(IDs)

    display(['processing ID ' IDs{id}]);

    %% load data

    load(fullfile(pn.dataIn, [IDs{id}, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat']), 'data');

    %% save TrlInfo from FT
    
    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;
    
    %% CSD transform (compute prior to averaging)
    
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    
    %% 8 Hz low-pass filter
    
    cfg = [];
    cfg.lpfilter = 'yes';
    cfg.lpfreq = 8;
    cfg.lpfiltord = 6;
    cfg.lpfilttype = 'but';
    cfg.lpfiltdir = 'twopass';
    
    data = ft_preprocessing(cfg,data);
    
    %% re-segment data with respect to stim onset, probe onset & response 
    % note that ft_redefinetrial unfortunately doesn't work due to the
    % time-locked structure
    
    % stim onset
    
    timepoint = repmat(3, size(data.trial,1),1);
    time = repmat(data.time{1}, size(data.trial,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-500;
    cfg.endsample = idx+2000;
    dataStim = ft_redefinetrial(cfg, data);
    
    % baseline: average of -250 to 0 ms prior to stim onset
    
    cfg = [];
    cfg.baseline = [2.75 3];
    [dataStim] = ft_timelockbaseline(cfg, dataStim);
    dataStim.time = -500*2:2:2000*2; % correct timing
    
    %% baseline: average of -250 to 0 ms prior to probe onset
    
    cfg = [];
    cfg.baseline = [5.75 6];
    [data] = ft_timelockbaseline(cfg, data);
    
    % probe onset
    
    timepoint = repmat(6.04, size(data.trial,1),size(data.time,2));
    time = repmat(data.time, size(data.trial,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-500;
    cfg.endsample = idx+500;
    dataProbe = data;
    dataProbe.trial = NaN(size(data.trial,1),60,cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,1)
        dataProbe.trial(indTrial,:,:) = data.trial(indTrial,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
    end
    dataProbe.time = -500*2:2:500*2;
    
    % response
    
    timepoint = repmat(6.04+TrlInfo(:,9), 1,size(data.time,2));
    time = repmat(data.time, size(data.trial,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-500;
    cfg.endsample = idx+500;
    trialToRemove = find(cfg.begsample<=0 | cfg.endsample>size(data.trial,3));
    dataResponse = data;
    dataResponse.trial = NaN(size(data.trial,1),60,cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,1)
        if ismember(indTrial, trialToRemove)
            dataResponse.trial(indTrial,:,:) = NaN;
        else
        	dataResponse.trial(indTrial,:,:) = data.trial(indTrial,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
        end
    end
    dataResponse.time = -500*2:2:500*2;
    
    % remove empty trials from all sturctures
    
    dataStim.trial(trialToRemove,:,:) = [];
    dataProbe.trial(trialToRemove,:,:) = [];
    dataResponse.trial(trialToRemove,:,:) = [];
    TrlInfo(trialToRemove,:) = [];
        
%% timelock to different events
    
    for indCond = 1:4
        for indCorrect = 1:2
            cfg = [];
            cfg.trials = TrlInfo(:,8)==indCond & TrlInfo(:,10) == indCorrect-1;
            % timelock to stim onset
            dataStimAvg{indCond,indCorrect} = ft_timelockanalysis(cfg, dataStim);
            % timelock to probe onset
            dataProbeAvg{indCond,indCorrect} = ft_timelockanalysis(cfg, dataProbe);
            % timelock to response
            dataResponseAvg{indCond,indCorrect} = ft_timelockanalysis(cfg, dataResponse);
        end
    end
    
    %% save computed files

    save(fullfile(pn.out, [IDs{id}, '_ERP.mat']), 'dataStimAvg', 'dataProbeAvg', 'dataResponseAvg');
    
end % ID
