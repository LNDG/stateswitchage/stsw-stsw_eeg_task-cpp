% Create across-subject averages by condition and group

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))

pn.root     = pwd;
pn.tools	= fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v5');

%% create or load grand averages by group

file_grandavg = fullfile(pn.out, 'dataStimGrandAvg_v5.mat');

if ~exist(file_grandavg)

    disp('create and save grand averages')
    
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

    ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
    ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

    for id = 1:length(IDs)
        load(fullfile(pn.out, [IDs{id}, '_ERP.mat']), 'dataStimAvg', 'dataProbeAvg', 'dataResponseAvg');
        ERPstruct.dataStimAvg(:,:,id) = dataStimAvg;
        ERPstruct.dataProbeAvg(:,:,id) = dataProbeAvg;
        ERPstruct.dataResponseAvg(:,:,id) = dataResponseAvg;
    end

    cfg = [];
    for indAge = 1:2
        for indCond = 1:4
            for indAcc = 1:2
                cfg.keepindividual = 'yes';
                dataStimGrandAvg{indCond,indAcc,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataStimAvg{indCond,indAcc,ageIdx{indAge}});
                dataProbeGrandAvg{indCond,indAcc,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataProbeAvg{indCond,indAcc,ageIdx{indAge}});
                dataResponseGrandAvg{indCond,indAcc,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvg{indCond,indAcc,ageIdx{indAge}});
            end
        end
    end

    %% save grand averages

    save(fullfile(pn.out, 'dataStimGrandAvg_v5.mat'), 'dataStimGrandAvg', 'IDs', 'ageIdx')
    save(fullfile(pn.out, 'dataProbeGrandAvg_v5.mat'), 'dataProbeGrandAvg', 'IDs', 'ageIdx')
    save(fullfile(pn.out, 'dataResponseGrandAvg_v5.mat'), 'dataResponseGrandAvg', 'IDs', 'ageIdx')
    
else
    disp('grand averages already exist')
end