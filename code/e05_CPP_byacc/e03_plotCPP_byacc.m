%% initialize

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))

pn.root     = pwd;
pn.tools	= fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools,'shadedErrorBar'));
    addpath(fullfile(pn.tools,'BrewerMap'));
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v5');
pn.plotFolder = fullfile(pn.root, 'figures', 'E');

%% load grand averages

load(fullfile(pn.out, 'dataStimGrandAvg_v5.mat'), 'dataStimGrandAvg', 'IDs', 'ageIdx')
load(fullfile(pn.out, 'dataProbeGrandAvg_v5.mat'), 'dataProbeGrandAvg')
load(fullfile(pn.out, 'dataResponseGrandAvg_v5.mat'), 'dataResponseGrandAvg')

%% plot l1, l4 correct/incorrect for younger and older adults

cBrew = brewermap(4,'RdBu');

ageGroups = {'YA'; 'OA'};

l1 = []; l2 = []; l3 = []; l4 = [];
h = figure('units','normalized','position',[.1 .1 .5 .35]);
for indAge = 1:2
    subplot(1,2,indAge)
    for indAcc = 1:2
        hold on;
        cBrew = brewermap(4,'RdBu');
        catAll = cat(4,dataResponseGrandAvg{1,indAcc,indAge}.individual,...
        dataResponseGrandAvg{2,indAcc,indAge}.individual,dataResponseGrandAvg{3,indAcc,indAge}.individual,...
        dataResponseGrandAvg{4,indAcc,indAge}.individual);

        condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
        curData = squeeze(catAll(:,54,:,1));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1{indAcc} = shadedErrorBar(dataResponseGrandAvg{1,indAcc,1}.time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indAcc,:),'linewidth', 2}, 'patchSaturation', .25);

        curData = squeeze(catAll(:,54,:,4));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        l4{indAcc} = shadedErrorBar(dataResponseGrandAvg{4,indAcc,1}.time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indAcc,:),'linewidth', 2, 'LineStyle', '--'}, 'patchSaturation', .25);
    end
    ll1  = line([ 0 0], [ -4*10^-4 12*10^-4], 'LineStyle', ':', 'LineWidth', 2);
    ll2  = line([ -1000 100], [ 0 0], 'LineStyle', ':', 'LineWidth', 2);
    set(ll1, 'Color', 'k')
    set(ll2, 'Color', 'k')
    ylim([-1.5*10^-4 12*10^-4])
    title(ageGroups{indAge})
    legend([l1{1}.mainLine, l1{2}.mainLine],...
                {'Incorrect'; 'Correct'}, 'location', 'NorthWest', 'Orientation', 'vertical'); legend('boxoff');
    xlim([-1000 100]); xlabel('Time (ms); response-locked'); ylabel('ERP Amplitude @POz (?V)');  
        set(findall(gcf,'-property','FontSize'),'FontSize',20)
end