currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

pn.root = pwd;
pn.tools = fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v3');
pn.plotFolder = fullfile(pn.root, 'figures', 'E');
    
addpath(fullfile(pn.root, 'tools'));
    % requires convertPtoExponential
    % requires mysigstar_vert
addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'));
addpath(fullfile(pn.root, 'tools', 'BrewerMap'));
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));

load(fullfile(pn.out, 'GrandAverages_v3.mat'), 'dataResponseGrandAvg', 'IDs', 'ageIdx')

%% first for younger adults

indAge = 1;
    
    %% individual threshold fits (CPP-based)

    for indCond = 1:4
        curTime = dataResponseGrandAvg{indCond,indAge}.time;
        dataForThreshold{indAge}(:,indCond,:) = ...
            squeeze(nanmean(nanmean(dataResponseGrandAvg{indCond,indAge}.individual(...
            :,54,dataResponseGrandAvg{1,1}.time>-50 & dataResponseGrandAvg{1,1}.time<50),2),3));
    end
    
    CPPthreshold.data = dataForThreshold{indAge};
    CPPthreshold.IDs = IDs(ageIdx{indAge});

    save(fullfile(pn.root, 'data', 'Z_CPPthreshold_YA.mat'), 'CPPthreshold');
    
    %% create RainCloudPlot version
    
    condPairs = [1,2; 2,3; 3,4];
    condPairsLevel = [];
    condPairsLevel{indAge} = [1 1 1]*10^-3;  
    lims{indAge} = [0 1.8]*10^-3;
    colorm = [.5,.5,.5];
    paramLabels = {'CPP: -50:50 ms [microVolt]'}; 

    h = figure('units','normalized','position',[.1 .1 .15 .15]);
    set(0, 'DefaultFigureRenderer', 'painters');    
    cla; hold on;

        dat = squeeze(dataForThreshold{indAge}(:,:));

     % read into cell array of the appropriate dimensions
        data = []; data_ws = [];
        for i = 1:4
            for j = 1:1
                data{i, j} = squeeze(dat(:,i));
                % individually demean for within-subject visualization
                data_ws{i, j} = dat(:,i)-...
                    nanmean(dat(:,:),2)+...
                    repmat(nanmean(nanmean(dat(:,:),2),1),size(dat(:,:),1),1);
            end
        end

        % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

        cl = colorm(1,:);

        box off
        cla;
            h_rc = rm_raincloud(data_ws, cl,1);
            % add stats
            for indPair = 1:size(condPairs,1)
                % significance star for the difference
                [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
                % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
                % sigstars on top
                if pval <.05
                   mysigstar_vert(gca, [condPairsLevel{indAge}(indPair), condPairsLevel{indAge}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

                end
            end
            view([90 -90]);
            axis ij
        box(gca,'off')
        %set(gca, 'YTick', [1,2,3,4]);
        set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
        ylabel('Target load'); xlabel(paramLabels{1})
        xlim(lims{indAge}); 
        curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
        
        % test linear effect
        curData = [data{1, 1}, data{2, 1}, data{3, 1}, data{4, 1}];
        X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
        [~, p, ci, stats] = ttest(IndividualSlopes);
        title({'CPP threshold'; ['linear effect: p = ', num2str(round(p,3))]}); 
        set(findall(gcf,'-property','FontSize'),'FontSize',20)

        figureName = 'CPP_threshold_YA';
        saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
        saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
        saveas(h, fullfile(pn.plotFolder, figureName), 'png');
        
        % check linear effect
        
        X = [1 1; 1 2; 1 3; 1 4]; b=X\dat'; IndividualSlopes_load = b(2,:);
        [~, p] = ttest(IndividualSlopes_load)
        % p = 0.6884
        
        %% SourceData
        
        SourceData = squeeze(dataForThreshold{indAge}(:,:));

%% repeat for older adults

indAge = 2;
    
    %% individual threshold fits (CPP-based)

    for indCond = 1:4
        curTime = dataResponseGrandAvg{indCond,indAge}.time;
        dataForThreshold{indAge}(:,indCond,:) = ...
            squeeze(nanmean(nanmean(dataResponseGrandAvg{indCond,indAge}.individual(...
            :,54,dataResponseGrandAvg{1,1}.time>-50 & dataResponseGrandAvg{1,1}.time<50),2),3));
    end
    
    CPPthreshold.data = dataForThreshold{indAge};
    CPPthreshold.IDs = IDs(ageIdx{indAge});

    save(fullfile(pn.root, 'data', 'Z_CPPthreshold_OA.mat'), 'CPPthreshold');
    
    %% create RainCloudPlot version
    
    condPairs = [1,2; 2,3; 3,4];
    condPairsLevel = [];
    condPairsLevel{indAge} = [1 1 1]*10^-3;  
    lims{indAge} = [0 1.8]*10^-3;
    colorm = [.5,.5,.5];
    paramLabels = {'CPP: -50:50 ms [microVolt]'}; 

    h = figure('units','normalized','position',[.1 .1 .15 .15]);
    set(0, 'DefaultFigureRenderer', 'painters');    
    cla; hold on;

        dat = squeeze(dataForThreshold{indAge}(:,:));

     % read into cell array of the appropriate dimensions
        data = []; data_ws = [];
        for i = 1:4
            for j = 1:1
                data{i, j} = squeeze(dat(:,i));
                % individually demean for within-subject visualization
                data_ws{i, j} = dat(:,i)-...
                    nanmean(dat(:,:),2)+...
                    repmat(nanmean(nanmean(dat(:,:),2),1),size(dat(:,:),1),1);
            end
        end

        % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

        cl = colorm(1,:);

        box off
        cla;
            h_rc = rm_raincloud(data_ws, cl,1);
            % add stats
            for indPair = 1:size(condPairs,1)
                % significance star for the difference
                [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
                % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
                % sigstars on top
                if pval <.05
                   mysigstar_vert(gca, [condPairsLevel{indAge}(indPair), condPairsLevel{indAge}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

                end
            end
            view([90 -90]);
            axis ij
        box(gca,'off')
        %set(gca, 'YTick', [1,2,3,4]);
        set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
        ylabel('Target load'); xlabel(paramLabels{1})
        set(findall(gcf,'-property','FontSize'),'FontSize',20)
        xlim(lims{indAge}); 
        curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
        
        figureName = 'CPP_threshold_OA';
        saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
        saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
        saveas(h, fullfile(pn.plotFolder, figureName), 'png');
        
        % check linear effect
        
        X = [1 1; 1 2; 1 3; 1 4]; b=X\dat'; IndividualSlopes_load = b(2,:);
        [~, p] = ttest(IndividualSlopes_load)
        % p = 1.1282e-05
        
        %% SourceData
        
        SourceData = squeeze(dataForThreshold{indAge}(:,:));
