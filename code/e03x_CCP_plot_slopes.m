%% plot CPP slope - HDDM drift correlations

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

pn.root     = pwd;
pn.tools	= fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v3');

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

%% load grand averages by group

disp('load grand averages')
load(fullfile(pn.out, 'GrandAverages_v3.mat'), 'dataStimGrandAvg', 'dataProbeGrandAvg', 'dataResponseGrandAvg')

%% individual slope fits

% slope fit from -250 to -100 (McGovern et al., 2018)

dataForSlopeFit = []; slopes = [];
for indAge = 1:2
    for indCond = 1:4
        dataForSlopeFit{indAge}(:,indCond,:) = squeeze(dataResponseGrandAvg{indCond,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-250 & dataResponseGrandAvg{1,1}.time<-100));
        for indID = 1:size(dataForSlopeFit{indAge},1)
            %slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),zscore(squeeze(dataForSlopeFit{indAge}(indID,indCond,:)))',1);
            slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),squeeze(dataForSlopeFit{indAge}(indID,indCond,:))',1);
            %scatter(1:size(dataForSlopeFit{indAge},3), zscore(squeeze(dataForSlopeFit{indAge}(indID,indCond,:)))', 'filled'); pause(.2)
        end
    end
end

figure; 
subplot(3,2,1); bar([nanmean(squeeze(slopes{1}(:,:,1)),1)]); title('Slope YA')
subplot(3,2,2); bar([nanmean(squeeze(slopes{1}(:,:,2)),1)]); title('Intercept YA')
subplot(3,2,3); bar([nanmean(squeeze(slopes{2}(:,:,1)),1)]); title('Slope OA')
subplot(3,2,4); bar([nanmean(squeeze(slopes{2}(:,:,2)),1)]); title('Intercept OA')
suptitle({'CPP slopes and intercepts -300:0 (cf. McGovern et al.)'})
subplot(3,2,[5 6]); hold on;
for indCond = 1:4
    scatter(squeeze(slopes{1}(:,indCond,1)),squeeze(slopes{1}(:,indCond,2)), 'filled');
end
title({'Note that CPP slope is inter-individually';'strongly correlated with CPP magnitude @ -300'})
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend({'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'})

% correlate with individual drift rate estimates

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_v12_a2_t2.mat')

idx_YA = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{1}));
idx_OA = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{2}));

figure; 
subplot(2,2,1); hold on;
for indCond = 1:4
    l{indCond} = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,indCond),squeeze(slopes{1}(:,indCond,1)), 'filled'); lsline();
end; title('CPP Slope YA')
subplot(2,2,2); hold on;
for indCond = 1:4
    scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,indCond),squeeze(slopes{2}(:,indCond,1)), 'filled'); lsline();
end; title('CPP Slope OA')
subplot(2,2,3); hold on;
for indCond = 1:4
    scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,indCond),squeeze(slopes{1}(:,indCond,2)), 'filled'); lsline();
end; title('CPP Intercept YA')
subplot(2,2,4); hold on;
for indCond = 1:4
    scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,indCond),squeeze(slopes{2}(:,indCond,2)), 'filled'); lsline();
end; title('CPP Intercept OA')
legend([l{1},l{2},l{3},l{4}], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'});
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% figure showcasing intercept-change correlation at inter-individual level

h = figure('units','normalized','position',[.1 .1 .4 .7]);

% anticorrelation of DDM estimates

subplot(4,2,1);
    a = HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1);
    b = HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1);
    l1 = scatter(a,b, 'k', 'filled'); lsline();
    [r, p] = corrcoef(a,b);
    legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
    title('YA: DDM drift intercept-change')
    xlabel('HDDM drift (EEG) L1'); ylabel('HDDM drift (EEG) L4-1'); 
subplot(4,2,2);
    a = HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1);
    b = HDDM_summary_v12_a2_t2.driftEEG(idx_OA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1);
    l1 = scatter(a,b, 'k', 'filled'); lsline();
    [r, p] = corrcoef(a,b);
    legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
    title('OA: DDM drift intercept-change')
    xlabel('HDDM drift (EEG) L1'); ylabel('HDDM drift (EEG) L4-1'); 

% Anticorrelation is also present at the level of the CPP. Largest drift decrease for those subjects with high baseline drift.

subplot(4,2,3);
    a = HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1);
    b = squeeze(slopes{1}(:,4,1))-squeeze(slopes{1}(:,1,1));
    l1 = scatter(a,b, 'k', 'filled'); lsline();
    [r, p] = corrcoef(a,b);
    legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
    title('YA: CPP drift intercept-change')
    xlabel('CPP slope (EEG) L1'); ylabel('CPP slope (EEG) L4-1'); 
subplot(4,2,4);
    a = HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1);
    b = squeeze(slopes{2}(:,4,1))-squeeze(slopes{2}(:,1,1));
    l1 = scatter(a,b, 'k', 'filled'); lsline();
    [r, p] = corrcoef(a,b);
    legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
    title('OA: CPP drift intercept-change')
    xlabel('CPP slope (EEG) L1'); ylabel('CPP slope (EEG) L4-1'); 

% correlation between drift changes in DDM estimate and CPP slope
subplot(4,2,5);
    a = HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1);
    b = squeeze(slopes{1}(:,1,1));
    l1 = scatter(a,b, 'k', 'filled'); lsline();
    [r, p] = corrcoef(a,b);
    legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
    title('YA:  DDM & CPP drift intercept correlate')
    xlabel('HDDM drift (EEG) L1'); ylabel('CPP slope (EEG) L1'); 
subplot(4,2,6);
    a = HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1);
    b = squeeze(slopes{2}(:,1,1));
    l1 = scatter(a,b, 'k', 'filled'); lsline();
    [r, p] = corrcoef(a,b);
    legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
    title('OA: DDM & CPP drift intercept correlate')
    xlabel('HDDM drift (EEG) L1'); ylabel('CPP slope (EEG) L1'); 

subplot(4,2,7);
    a = HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1);
    b = squeeze(slopes{1}(:,4,1))-squeeze(slopes{1}(:,1,1));
    l1 = scatter(a,b, 'k', 'filled'); lsline();
    [r, p] = corrcoef(a,b);
    legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
    title('YA:  DDM & CPP drift intercept-change correlate')
    xlabel('HDDM drift (EEG) L4-L1'); ylabel('CPP slope (EEG) L4-1'); 
subplot(4,2,8);
    a = HDDM_summary_v12_a2_t2.driftEEG(idx_OA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1);
    b = squeeze(slopes{2}(:,4,1))-squeeze(slopes{2}(:,1,1));
    l1 = scatter(a,b, 'k', 'filled'); lsline();
    [r, p] = corrcoef(a,b);
    legend(l1, ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]); legend('boxoff')
    title('OA: DDM & CPP drift intercept-change correlate')
    xlabel('HDDM drift (EEG) L4-L1'); ylabel('CPP slope (EEG) L4-1'); 

    set(findall(gcf,'-property','FontSize'),'FontSize',14)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/'; mkdir(pn.plotFolder);
figureName = 'E_DriftCorrelation_CPP_HDDM';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% try to show association via neural time series

[~, sortInd] = sort(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), 'descend');

% add within-subject error bars, do stats between fast and slow performers

h = figure('units','normalized','position',[.1 .1 .4 .4]);
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
xlim([-1000 350])
legend({'Fast L1'; 'Slow L1'; 'Fast L4'; 'Slow L4'}, 'location', 'NorthWest'); legend('boxoff');
title('CPP median split according to high/low L1 DDM drift')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E/'; mkdir(pn.plotFolder);
figureName = 'E_MedianSplitFastSlow';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

figure;
subplot(2,1,1);
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    legend({'L1', 'L2', 'L3', 'L4'}); legend('boxoff');
    title('CPP median split according to high/low L1 DDM drift')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(2,1,2);
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)

figure;
subplot(4,1,1);
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    legend({'Fast'; 'Slow'}); legend('boxoff');
    title('Load 1')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(4,1,2);
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    title('Load 2')
subplot(4,1,3);
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    title('Load 3')
subplot(4,1,4);
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    title('Load 4')
suptitle('CPP median split according to high/low L1 DDM drift')

figure;
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(1:10),54,:),1)))
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(30:end),54,:),1)))
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(1:10),54,:),1)))
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(30:end),54,:),1)))
legend({'Fast L1'; 'Slow L1'; 'Fast L4'; 'Slow L4'});


figure;
subplot(2,1,1);
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    legend({'Fast L1'; 'Slow L1'; 'Fast L4'; 'Slow L4'}); legend('boxoff');
    title('CPP median split according to high/low L1 DDM drift')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(2,1,2);
    hold on; plot(dataResponseGrandAvg{1,1}.time, ...
        squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:)-...
        dataResponseGrandAvg{1,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, ...
       squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:)-...
       dataResponseGrandAvg{1,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    legend({'Fast L1'; 'Slow L1'}); legend('boxoff');
    title('CPP median split according to high/low L1 DDM drift')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% OAs

[~, sortInd] = sort(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1), 'descend');

% OAs may also modulate their threshold

figure;
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
legend({'Fast L1'; 'Slow L1'; 'Fast L4'; 'Slow L4'}); legend('boxoff');
title('CPP median split according to high/low L1 DDM drift')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figure;
imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{1,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:)))
imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{4,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:)))
imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{1,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:))-...
    squeeze(dataResponseGrandAvg{4,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:)))

figure;
subplot(2,1,1);
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
    legend({'L1', 'L2', 'L3', 'L4'}); legend('boxoff');
    title('CPP median split according to high/low L1 DDM drift')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(2,1,2);
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)


%% Slope-drift L1 intercorrelations

figure;
subplot(1,2,1); hold on;
    l1 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1),squeeze(slopes{1}(:,1,1)), 'filled'); lsline();
    l2 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1),squeeze(slopes{2}(:,1,1)), 'filled'); lsline();
    [r,p] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1),squeeze(slopes{1}(:,1,1)));
    Corr1 = r(2); P1 = p(2);
    [r,p] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1),squeeze(slopes{2}(:,1,1)));
    Corr2 = r(2); P2 = p(2);
    legend([l1,l2], {['YA: r = ', num2str(round(Corr1,2)), '; p = ', num2str(round(P1,3))],...
        ['OA: r = ', num2str(round(Corr2,2)), '; p = ' num2str(round(P2,3))]})
    title('CPP slope correlates with estimated drift in Load 1')
    xlabel('Drift estimate'); ylabel('CPP Slope -300 to 0');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,2,2); hold on;
    l1 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1),squeeze(slopes{1}(:,1,2)), 'filled'); lsline();
    l2 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1),squeeze(slopes{2}(:,1,2)), 'filled'); lsline();
    [r,p] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1),squeeze(slopes{1}(:,1,2)));
    Corr1 = r(2); P1 = p(2);
    [r,p] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1),squeeze(slopes{2}(:,1,2)));
    Corr2 = r(2); P2 = p(2);
    legend([l1,l2], {['YA: r = ', num2str(round(Corr1,2)), '; p = ', num2str(round(P1,3))],...
        ['OA: r = ', num2str(round(Corr2,2)), '; p = ' num2str(round(P2,3))]})
    title('CPP intercept correlates with estimated drift in Load 1')
    xlabel('Drift estimate'); ylabel('CPP Intercept @ -300');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% get peak as threshold estimate

CPPthreshold = [];
for indAge = 1:2
    for indCond = 1:4
        CPPthreshold{indAge}(:,indCond) = squeeze(nanmean(dataResponseGrandAvg{indCond,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<100),3));
    end
end

figure; 
bar([nanmean(squeeze(CPPthreshold{1}(:,:)),1), nanmean(squeeze(CPPthreshold{2}(:,:)),1)]); title('Thresholds YA, OA')

figure; 
subplot(1,2,1); hold on;
for indCond = 1:4
    l{indCond} = scatter(HDDM_summary_v12_a2_t2.thresholdEEG(idx_YA,indCond),squeeze(CPPthreshold{1}(:,indCond)), 'filled'); lsline();
end; title('CPP Threshold YA')
subplot(1,2,2); hold on;
for indCond = 1:4
    scatter(HDDM_summary_v12_a2_t2.thresholdEEG(idx_OA,indCond),squeeze(CPPthreshold{2}(:,indCond)), 'filled'); lsline();
end; title('CPP Threshold OA')
legend([l{1},l{2},l{3},l{4}], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'});
set(findall(gcf,'-property','FontSize'),'FontSize',18)
