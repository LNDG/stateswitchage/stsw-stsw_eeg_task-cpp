currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))

pn.root = pwd;
pn.tools = fullfile(pn.root, 'tools');
    addpath(fullfile(pn.tools,'fieldtrip')); ft_defaults;
pn.out	= fullfile(pn.root, 'data', 'E2_CCP_v3');
pn.plotFolder = fullfile(pn.root, 'figures', 'E');
    
addpath(fullfile(pn.root, 'tools'));
    % requires convertPtoExponential
    % requires mysigstar_vert
addpath(fullfile(pn.root, 'tools', 'shadedErrorBar'));
addpath(fullfile(pn.root, 'tools', 'BrewerMap'));
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));

load(fullfile(pn.out, 'GrandAverages_v3.mat'), 'dataResponseGrandAvg', 'IDs', 'ageIdx')

%% plot stim-locked CPP

h = figure('units','normalized','position',[.1 .1 .75 .35]);
set(0, 'DefaultFigureRenderer', 'painters');
subplot(1,3,1)
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.zlim = [-6*10^-4 6*10^-4];
    cfg.colormap = cBrew;
    cfg.marker = 'off';
    cfg.highlight = 'on';
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 50;
    cfg.highlightchannel = dataStimGrandAvg{1}.label{54};
    cfg.figure = h;
    plotData = [];
    plotData.label = dataStimGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,1}.time>1000 & dataStimGrandAvg{1,1}.time<3000;
    grandAvg = cat(4, dataStimGrandAvg{1,1}.individual, dataStimGrandAvg{2,1}.individual, ...
        dataStimGrandAvg{3,1}.individual, dataStimGrandAvg{4,1}.individual);
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(grandAvg(:,:,idxTime,:),3),1),4))';
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','ERP amplitude (?V)');
    title({'ERP during dynamic stimulus presentation'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    figureName = 'E1_CPP_YA_StimPresentation_p1';
    saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
    
h = figure('units','normalized','position',[.1 .1 .75 .35]);
set(0, 'DefaultFigureRenderer', 'painters');

subplot(1,3,[2,3]); cla; hold on;
    
    % highlight different experimental phases in background
    patches.timeVec = [3 6.066]-3;
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-1 1];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    
    time = dataStimGrandAvg{1,1}.time;
    time = time./1000;

    cBrew = brewermap(5,'greys');
    cBrew = cBrew(2:end,:);

    catAll = cat(4,dataStimGrandAvg{1,1}.individual,...
    dataStimGrandAvg{2,1}.individual,dataStimGrandAvg{3,1}.individual,...
    dataStimGrandAvg{4,1}.individual);

% new value = old value ? subject average + grand average

    condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
    curData = squeeze(catAll(:,54,:,1));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);

    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine],...
            {'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', ...
            'South', 'orientation', 'horizontal'); legend('boxoff');
    xlim([-.5 4]); ylim([-8 8]*10^-4)
    xlabel('Time (ms from stimulus onset); response-locked'); ylabel('ERP Amplitude @POz (?V)');
    title({'CPP does not increase during stimulus processing'})

    set(findall(gcf,'-property','FontSize'),'FontSize',18)

    figureName = 'E1_CPP_YA_StimPresentation_p2';
    saveas(h, fullfile(pn.plotFolder, figureName), 'fig');
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');