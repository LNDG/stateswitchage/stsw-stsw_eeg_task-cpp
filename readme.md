[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Centro-parietal positivity

The CPP is an electrophysiological signature of internal evidence-to-bound accumulation28,73,82. We probed the task modulation of this established signature and assessed its convergence with behavioral parameter estimates. To derive the CPP, preprocessed EEG data were low-pass filtered at 8 Hz with a sixth-order Butterworth filter to exclude low-frequency oscillations, epoched relative to response, and averaged across trials within each condition. In accordance with the literature, this revealed a dipolar scalp potential that exhibited a positive peak over parietal channel POz (see Fig. 2). We temporally normalized individual CPP estimates to a condition-specific baseline during the final 250 ms preceding probe onset. As a proxy of evidence drift rate, CPP slopes were estimates via linear regression from −250 to −100 ms surrounding response execution, while the average CPP amplitude from −50 to 50 ms served as an indicator of decision thresholds (i.e., boundary separation; e.g., McGovern et al. (2018)).

To investigate whether a similar “ramping” potential was observed during stimulus presentation, we aligned data to stimulus onset and temporally normalized signals to the condition-specific signal during the final 250 ms prior to stimulus onset. During stimulus presentation, no “ramp”-like signal or load modulation was observed at the peak CPP channel. This suggests that immediate choice requirements were necessary for the emergence of the CPP, although prior work has shown the CPP to be independent of explicit motor requirements (O’Connell et al., 2012).

Finally, we assessed whether differences between probed stimulus attributes could account for load-related CPP changes (Supplementary Fig. 2e–g). For this analysis, we selected trials separately by condition and probed attribute. Note that for different probes (but not cues), trials were uniquely associated with each feature and trial counts were approximately matched across conditions. We explored differences between different conditions via paired t-tests. To assess load effects on CPP slopes and thresholds as a function of probed attribute, we calculated firstlevel load effects by means of a linear model and assessed their difference from zero via paired t-tests.

**References**

McGovern, D. P., Hayes, A., Kelly, S. P. & O’Connell, R. G. Reconciling age-related changes in behavioural and neural indices of human perceptual decision-making. Nat. Hum. Behav. 2, 955–966 (2018).

O’Connell, R. G., Dockree, P. M. & Kelly, S. P. A supramodal accumulationto-bound signal that determines perceptual decisions in humans. Nat. Neurosci. 15, 1729–1735 (2012).

## Function overview

v3: single-trial LPF at 8 Hz, by condition

* **E1_CCP_v3**

    ```
    - CSD transform
    - 8 Hz low-pass filter (6th order twopass Butterworth)
    - dataStimAvg: stim-locked, bl average of -250 to 0 ms prior to stim onset
    - dataProbeAvg: probe-locked, bl average of-250 to 0 ms prior to probe onset
    - dataResponseAvg: rsp-locked, bl average of-250 to 0 ms prior to probe onset
    - average within load condition
    ```

* **E1b_CCP_create_avg_v3**

    - create group averages

* **e03_CPP_OverviewFigure_**

    - figure of CPP at POz by load
    - inset 1: raincloudplot of extracted slopes (-250 ms to -50 ms pre-rsp)
    - inset 2: topography (-250 ms to 100 ms pre-rsp)

* *e03s_boundarySeparation*
    - threshold/boundary separation as a proxy ov avg. CPP -50 to 50 ms peri-rsp
    - RCP of threshold (YA, OA)

* *e03s_CPP_stimlocked_*
    - CPP locked to stimulus & probe onset to investigate "ramping" pre-probe

* *e03x_CPP_driftcorr_mdsplit*
    - plot correlation with drift rates (L1, YA, OA)
    - plot median split for L1 and L4 (YA, OA)

**e05_CPP_byacc**

    - unused: plot CPP split by correct/incorrect responses

**e06_CPP_bystim**

    - v4: single-trial LPF at 8 Hz, by condition and stimulus, this leads to non-attractive ERPs
    - YA reported in NatComms supplement